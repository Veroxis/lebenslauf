# Lebenslauf

## Persönliche Daten

![Profile Picture](media/profile.png)

### Name

Daniel Kupczak

### Geburtsdatum

15.11.1994 in Bad Hersfeld

### E-Mail

<d.kupczak94@gmail.com>

### Telefon

0176 / 24080876

### Adresse

Kuthstraße 36

51107 Köln

### Nationalität

Deutsch

### Familienstand

Ledig

## Berufstätigkeit

### Seit 01/2020

**TWT reality bytes GmbH** (Köln)

Webdeveloper und DevOps

## Ausbildung

### 08/2016 - 01/2020

**TWT reality bytes GmbH** (Köln)

Ausbildung zum Fachinformatiker für Anwendungsentwicklung

### 08/2014 - 07/2016

**Konrad-Zuse-Schule** (Hünfeld)

Ausbildung zum Informationstechnischen Assistenten

inklusive Fachhochschulreife

### 08/2013 - 03/2014

**Metallverarbeitung Friedewald GmbH** (Friedewald)

Ausbildung zum Metallbauer für Konstruktionstechnik (abgebrochen)

### 08/2012 - 07/2013

**Euro Fassadentechnik GmbH** (Bad Hersfeld)

Ausbildung zum Technischen Systemplaner (abgebrochen)

## Schule

### 2005 - 2011

**Gesamtschule Obersberg** (Bad Hersfeld)

Mittlere Reife

### 2001 - 2005

**Grundschule an der Sommerseite** (Bad Hersfeld)

## Aufgabenbereiche

### TWT reality bytes GmbH

Als Webdeveloper:

- PHP Entwickler
- Symfony Entwickler
- Diverse Scripts zur Automatisierung von ansonsten langwierigen Prozessen

Als DevOp:

- Aufsetzen von Projekten, für gewöhnlich in Docker Umgebungen
- Automatisierung von Deployment Prozessen
- Monitoring von Systemen und Anwendungen via Prometheus und Grafana
- Verwaltung der internen Gitlab Instanz
- Serververwaltung via Ansible
- Supporter für Arbeitskollegen die Probleme mit ihren Geräten haben

# {#id .pagebreak}

## Kenntnisse und Interessen

### Fremdsprachen

- Deutsch (Muttersprache)
- Englisch (Flüssig)

### GNU/Linux

- Debian, Ubuntu und CentOS im Serverbetrieb
- Debian, Ubuntu und Arch Linux als Workstation
- Fundierte Kenntnisse über den Aufbau von GNU/Linux Systemen durch zahlreiche individuelle Konfigurationen von Arch Linux

### Programmier und Scriptsprachen

- PHP
- Bash
- Rust
- Go
- C++
- Python
- C#
- C

### Datenbanken

- MariaDB
- SQLite
- MySQL

### Beispiele privater Bastelprojekte

- Modding eines Thinkpad T420:
  - Einbau eines 1440p Displays mithilfe eines Translationsboards
  - Ersetzen einiger alter Module durch modernere Versionen, zum Beispiel der Wireless Karte
  - Ersetzen des BIOS durch einen eigenen Build von Coreboot
  - Upgrade der CPU durch eine neuere Generation als mit dem ursprünglichen BIOS möglich
  - RAM Upgrade
  - 3 Festplatten, 2x 2,5 Zoll SSD und 1x mSata SSD
  - Jailbreaken des alten Motherboards, welches beim Kauf ein BIOS Passwort hatte
- Modding eines Thinkpad X220:
  - Ersetzen des BIOS durch einen eigenen Build von Coreboot
  - RAM Upgrade
- Diverse Android Smartphones:
  - Ersetzen des Hersteller OS durch alternative Systeme wie LineageOS oder AOSPE
- Reparatur alter GameBoy Cartridges durch das Ersetzen der CMOS Batterien (Löten)
- Reparatur der Switches an meiner Logitech G700 Maus (Löten)
