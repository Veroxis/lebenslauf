FROM debian:latest
RUN apt-get update && apt-get install -yyyy pandoc make wget wkhtmltopdf
RUN useradd --create-home 1000
RUN mkdir /source
RUN chown 1000:1000 /source
WORKDIR /source
